package com.br.puc.onemessage.schedule.data.requests;

public class QueueCorporateRequest {

	private String message;
	private String phone;

	public QueueCorporateRequest() {
		super();
	}

	public QueueCorporateRequest(String message, String phone) {
		super();
		this.message = message;
		this.phone = phone;

	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "QueueCorporateRequest [message=" + message + ", phone=" + phone + "]";
	}

}
