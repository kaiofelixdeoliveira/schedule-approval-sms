package com.br.puc.onemessage.schedule.data.sends;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.br.puc.onemessage.schedule.data.requests.QueueCorporateRequest;

@Service
public class SendMessage {

	private static final String DESTINATION_NAME = "corporate-queue";
	private static final Logger logger = LoggerFactory.getLogger(SendMessage.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	public void send(QueueCorporateRequest message) {
		logger.info("Enviando mensagem para a fila");
		jmsTemplate.convertAndSend(DESTINATION_NAME, message);
	}

}
