package com.br.puc.onemessage.schedule.data.tasks;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.schedule.core.utils.DateFormatUtil;
import com.br.puc.onemessage.schedule.data.requests.QueueCorporateRequest;
import com.br.puc.onemessage.schedule.data.sends.SendMessage;
import com.br.puc.onemessage.schedule.domain.entities.Campaign;
import com.br.puc.onemessage.schedule.domain.entities.Message;
import com.br.puc.onemessage.schedule.domain.usecases.MessageUseCase;

@Component
public class Scheduler {

	private final Logger LOGGER = LoggerFactory.getLogger(Scheduler.class);

	@Autowired
	private MessageUseCase messageService;

	
	
	@Scheduled(fixedDelay = 30000)
	public void fixedDelaySch() {

		LOGGER.info("Fixed Delay scheduler:[{}] ", DateFormatUtil.dateTimeNow());
		LOGGER.info("Procurando mensagens aptas para enviar ...");

		List<Message> messages = messageService.findAllApprovalMessages();
		LOGGER.info("Quantidade de mensagens aptas para envio: [{}]", messages.size());

		messageService.sendMessages(messages);

	}
}
