package com.br.puc.onemessage.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableJms
@SpringBootApplication
@EnableScheduling
public class ScheduleApprovalSmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScheduleApprovalSmsApplication.class, args);
	}

}
