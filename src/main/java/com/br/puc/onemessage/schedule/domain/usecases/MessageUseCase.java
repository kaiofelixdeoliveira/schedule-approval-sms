package com.br.puc.onemessage.schedule.domain.usecases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.schedule.core.utils.DateFormatUtil;
import com.br.puc.onemessage.schedule.data.repositories.MessageRepository;
import com.br.puc.onemessage.schedule.data.requests.QueueCorporateRequest;
import com.br.puc.onemessage.schedule.data.sends.SendMessage;
import com.br.puc.onemessage.schedule.domain.entities.Campaign;
import com.br.puc.onemessage.schedule.domain.entities.Message;

@Component
public class MessageUseCase {
	private final Logger LOGGER = LoggerFactory.getLogger(MessageUseCase.class);

	public MessageRepository messageRepository;

	@Autowired
	private SendMessage sendMessage;

	private static final String EMPTY = "empty";

	@Autowired
	public MessageUseCase(MessageRepository messageRepository) {
		this.messageRepository = messageRepository;
	}

	public List<Message> getMessages(String phone) {

		List<Message> messages = messageRepository.findByPhone(phone);

		if (messages == null) {
			return new ArrayList<>();
		}
		LOGGER.info("Using reactive repository");
		return messages;

	}

	public Message getMessage(Integer id) {
		Optional<Message> message = messageRepository.findById(id);
		message.isPresent();

		LOGGER.info("Using reactive repository");
		return message.isPresent() ? message.get() : new Message();

	}

	public void saveMessage(Message message) {

		if (message != null) {
			messageRepository.save(message);
			LOGGER.info("Mensagem salva com sucesso");

		}

	}

	public void saveMessages(List<Message> messages) {

		if (!messages.isEmpty()) {
			messageRepository.saveAll(messages);
			LOGGER.info("Mensagens salva com sucesso");
		}

	}

	public void deleteMessage(Integer id) throws Exception {

		if (id != null) {
			messageRepository.deleteById(id);

		} else {
			LOGGER.error("O id não pode ser nulo");
			throw new Exception("Problema ao tentar apagar o registro no banco");
		}

	}

	public void deleteMessage(Message message) throws Exception {

		if (message != null) {
			messageRepository.delete(message);

		} else {
			LOGGER.error("O Objeto message não pode ser nulo");
			throw new Exception("Problema ao tentar apagar o registro no banco");
		}

	}

	public List<Message> findAllApprovalMessages() {

		Boolean approval = true;
		Boolean isSend = false;

		List<Message> messages = messageRepository.findByIsApprovalAndIsSend(approval, isSend);
		if (messages != null) {
			return messages;

		}

		return new ArrayList<>();

	}

	public void sendMessages(List<Message> messages) {
		if (!messages.isEmpty()) {
			Iterator<Message> messagesIterator = messages.iterator();

			while (messagesIterator.hasNext()) {

				QueueCorporateRequest queueCorporateRequest = new QueueCorporateRequest();
				Message message = messagesIterator.next();
				if (!message.isSend()) {

					queueCorporateRequest.setMessage(message.getMessage());
					queueCorporateRequest.setPhone(message.getPhone());
					Campaign campaign = new Campaign();
					campaign = message.getCampaign();

					if ((campaign != null //
							&& !campaign.getName().equalsIgnoreCase(EMPTY))//
							&& (message.getDateTrigger()!=null //
							&& !message.getDateTrigger().equalsIgnoreCase(DateFormatUtil.dateNow()))) {//
						continue;
					}

					sendMessage.send(queueCorporateRequest);
					// seta mensagem como ja enviada
					message.setSend(true);
					message.setDateSend(DateFormatUtil.dateNow());
					saveMessage(message);

				}
			}
		}
	}

	public List<Message> findAllApprovalAndNotSystemicMessages() {

		Boolean approval = true;
		Boolean send = true;

		List<Message> messages = messageRepository.findByIsApprovalAndIsSend(approval, send);

		if (messages != null) {
			return messages;

		}

		return new ArrayList<>();

	}

	public void approvalMessage(Message message) {

		if (message != null && message.getId() != null) {
			Optional<Message> getMessage = messageRepository.findById(message.getId());

			if (getMessage.isPresent()) {
				getMessage.get().getApproval().setApproval(true);
				messageRepository.save(getMessage.get());
			}

		}

	}

	public void disapprovalMessage(Message message) {

		if (message != null && message.getId() != null) {
			Optional<Message> getMessage = messageRepository.findById(message.getId());

			if (getMessage.isPresent()) {
				getMessage.get().getApproval().setApproval(false);
				messageRepository.save(getMessage.get());
			}

		}

	}

}
